# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the cryptsetup-nuke-password package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: cryptsetup-nuke-password\n"
"Report-Msgid-Bugs-To: cryptsetup-nuke-password@packages.debian.org\n"
"POT-Creation-Date: 2019-07-05 15:24+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:1001
msgid "Nuke password:"
msgstr ""

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:1001
msgid ""
"If you setup a “nuke password”, you will be able to type this password at "
"the early-boot prompt asking your passphrase to unlock your luks-encrypted "
"partitions. Instead of decrypting the partitions, typing this password will "
"instead wipe the encryption keys from the luks container so that it is no "
"longer possible to unlock the encrypted partitions."
msgstr ""

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:1001
msgid ""
"This provides a relatively stealth way to make your data unreadable in case "
"you fear that your computer is going to be seized."
msgstr ""

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:1001
msgid ""
"If you want to cancel this operation or disable any nuke password already "
"configured, simply enter an empty password. If needed, you will be given the "
"option to pick between both choices."
msgstr ""

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:2001
msgid "Re-enter password to verify:"
msgstr ""

#. Type: password
#. Description
#: ../cryptsetup-nuke-password.templates:2001
msgid ""
"Please enter the same nuke password again to verify that you have typed it "
"correctly."
msgstr ""

#. Type: error
#. Description
#: ../cryptsetup-nuke-password.templates:3001
msgid "Password input error"
msgstr ""

#. Type: error
#. Description
#: ../cryptsetup-nuke-password.templates:3001
msgid "The two passwords you entered were not the same. Please try again."
msgstr ""

#. Type: select
#. Choices
#: ../cryptsetup-nuke-password.templates:4001
msgid "Keep the current password"
msgstr ""

#. Type: select
#. Choices
#: ../cryptsetup-nuke-password.templates:4001
msgid "Overwrite the current password"
msgstr ""

#. Type: select
#. Choices
#: ../cryptsetup-nuke-password.templates:4001
msgid "Remove the current password"
msgstr ""

#. Type: select
#. Description
#: ../cryptsetup-nuke-password.templates:4002
msgid "A nuke password is already configured, what should be done?"
msgstr ""

#. Type: select
#. Description
#: ../cryptsetup-nuke-password.templates:4002
msgid ""
"The existence of /etc/cryptsetup-nuke-password/password_hash suggests that a "
"nuke password is already configured. If you decide to keep the current nuke "
"password, nothing will be done. If you decide to overwrite the current nuke "
"password, you will be asked for a new password and it will replace the "
"currently configured one. If you decide to remove the nuke password, you "
"will not be asked for a new password and the currently configured password "
"will be disabled."
msgstr ""
