#!/bin/sh

# Configure the test suite to run against the files installed
# in the package
export CRYPT=/usr/lib/cryptsetup-nuke-password/crypt
export ASKPASS=/lib/cryptsetup/askpass

# Run the upstream test-suite
make check
